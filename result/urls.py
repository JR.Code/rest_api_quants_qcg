from django.urls import path
from . import views

urlpatterns = [
    path('result/<int:id>', views.getResult.as_view(), name='result')
]