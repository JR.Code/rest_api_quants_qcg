from secrets import choice
from webbrowser import get
from django.shortcuts import render

import json
import random

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

# Create your views here.
class getResult(APIView):

    permission_classes = (IsAuthenticated,)


    def get(self, request, id):
        #Send to API
        problems = ["MaxCut", "ExactCover", "MaxCut_Weighted", "JSP"]
        status = ["Solved", "Solving"]
        test_response = {"id": id, "problem": choice(problems), "status": choice(status), "solution": "0101100110"}
        return Response(test_response)