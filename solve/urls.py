from django.urls import path
from . import views

urlpatterns = [
    path('solve/', views.solveTask.as_view(), name='solve')
]
