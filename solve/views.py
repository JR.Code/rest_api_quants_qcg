from webbrowser import get
from django.shortcuts import render

import json
import random
from pprint import pprint

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated



class solveTask(APIView):
    permission_classes = (IsAuthenticated,)


    def get(self, request):
        return Response({"message": "Welcome"})

    def post(self, request):
        data = request.data
        algorithm = data["algorithm"]
        config = data["config"]
        input_data_list = data["data"]
        
        id = random.randint(0, 100)
        return Response(id)